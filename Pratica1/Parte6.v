module Parte6(SW, HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);
    input [17:0]SW;
    output [0:6]HEX0;
    output [0:6]HEX1;
    output [0:6]HEX2;
    output [0:6]HEX3;
    output [0:6]HEX4;
    output [0:6]HEX5;
    output [0:6]HEX6;
    output [0:6]HEX7;
 
    decodificador d1(SW[17:15], SW[14:0], HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7);
endmodule

module decodificador(s, c, d0, d1, d2, d3, d4, d5, d6, d7);
    //Chaves:
    input [2:0]s;
    input [14:0]c;
    
    //Displays fisicos
    output [6:0]d0;
    output [6:0]d1;
    output [6:0]d2;
    output [6:0]d3;
    output [6:0]d4;
    output [6:0]d5;
    output [6:0]d6;
    output [6:0]d7;
    
    //Representacao de caracteres
    wire [2:0]H = c[14:12];
    wire [2:0]E = c[11:9];
    wire [2:0]L = c[8:6];
    wire [2:0]L2 = c[5:3];
    wire [2:0]O = c[2:0];
    wire [2:0]X = 3'b111;

    //Displays logicos
    wire [2:0]H0;
    wire [2:0]H1;
    wire [2:0]H2;
    wire [2:0]H3;
    wire [2:0]H4;
    wire [2:0]H5;
    wire [2:0]H6;
    wire [2:0]H7;

    //Atribuicao de caracteres aos displays logicos
    mux_3bit_8to1 m1(X, X, X, H, E, L, L2, O, s, H7);
    mux_3bit_8to1 m2(X, X, H, E, L, L2, O, X, s, H6);
    mux_3bit_8to1 m3(X, H, E, L, L2, O, X, X, s, H5);
    mux_3bit_8to1 m4(H, E, L, L2, O, X, X, X, s, H4);
    mux_3bit_8to1 m5(E, L, L2, O, X, X, X, H, s, H3);
    mux_3bit_8to1 m6(L, L2, O, X, X, X, H, E, s, H2);
    mux_3bit_8to1 m7(L2, O, X, X,  X, H, E, L, s, H1);
    mux_3bit_8to1 m8(O, X, X, X, H, E, L, L2, s, H0);

    //Atrbuindo aos displays fisicos os valores dos displays logicos
    char_7seg c1(H7[2], H7[1], H7[0], d7);
    char_7seg c2(H6[2], H6[1], H6[0], d6);
    char_7seg c3(H5[2], H5[1], H5[0], d5);
    char_7seg c4(H4[2], H4[1], H4[0], d4);
    char_7seg c5(H3[2], H3[1], H3[0], d3);
    char_7seg c6(H2[2], H2[1], H2[0], d2);
    char_7seg c7(H1[2], H1[1], H1[0], d1);
    char_7seg c8(H0[2], H0[1], H0[0], d0);
endmodule

module char_7seg(c2, c1, c0, D);
    input c2, c1, c0;
    output [0:6]D;
    
    wire h = (~c2&~c1&~c0);
    wire e = (~c2&~c1&c0);
    wire l = (~c2&c1&~c0);
    wire o = (~c2&c1&c0);

    assign D[6] = ~(h|e);
    assign D[5] = ~(h|e|l|o);
    assign D[4] = ~(h|e|l|o);
    assign D[3] = ~(e|l|o);
    assign D[2] = ~(h|o);
    assign D[1] = ~(h|o);
    assign D[0] = ~(e|o);
endmodule

module mux_3bit_8to1(a, b, c, d, e, f, g, h, s, m);
    input [2:0]a;
    input [2:0]b;
    input [2:0]c;
    input [2:0]d;
    input [2:0]e;
    input [2:0]f;
    input [2:0]g;
    input [2:0]h;
    input [2:0]s;
    output [2:0]m;
 
    mult8p1 m1(a[2], b[2], c[2], d[2], e[2], f[2], g[2], h[2], s, m[2]);
    mult8p1 m2(a[1], b[1], c[1], d[1], e[1], f[1], g[1], h[1], s, m[1]);
    mult8p1 m3(a[0], b[0], c[0], d[0], e[0], f[0], g[0], h[0], s, m[0]);
endmodule

module mult8p1(a, b, c, d, e, f, g, h, s, m);
    input a, b, c, d, e, f, g, h;
    input [2:0]s;
    output m;
    
    mult2p1 m1(a, b, s[0], n0);
    mult2p1 m2(c, d, s[0], n1);
    mult2p1 m3(e, f, s[0], n2);
    mult2p1 m4(g, h, s[0], n3);
    mult2p1 m5(n0, n1, s[1], r0);
    mult2p1 m6(n2, n3, s[1], r1);
    mult2p1 m7(r0, r1, s[2], m);
endmodule

module mult2p1(x, y, s, m);
    input x, y, s;
    output m;
    
    assign m = (~s&x)|(s&y);
endmodule
