module Parte4(SW, HEX0);
    input [17:0]SW;
    output [0:6]HEX0;
    decodificador d1(SW[2], SW[1], SW[0], HEX0);
endmodule

module decodificador(c2, c1, c0, D);
    input c2;
    input c1;
    input c0;
    output [0:6]D;
    
    wire h = (~c2&~c1&~c0);
    wire e = (~c2&~c1&c0);
    wire l = (~c2&c1&~c0);
    wire o = (~c2&c1&c0);
    
    assign D[6] = ~(h|e);
    assign D[5] = ~(h|e|l|o);
    assign D[4] = ~(h|e|l|o);
    assign D[3] = ~(e|l|o);
    assign D[2] = ~(h|o);
    assign D[1] = ~(h|o);
    assign D[0] = ~(e|o);
endmodule
