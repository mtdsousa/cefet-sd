//Generate: http://www.asic-world.com/verilog/verilog2k2.html
module Parte2(SW, LEDR, LEDG);
    input [17:0]SW;
    output [17:0]LEDR;
    output [8:0]LEDG;

    genvar i;
    generate
        for (i=7; i >= 0; i=i-1) begin : mult2p1
            mult2p1(SW[i], SW[i+8], SW[17], LEDR[i+10]);
            mult2p1(SW[i], SW[i+8], SW[17], LEDG[i]);
        end
    endgenerate
endmodule

module mult2p1(x, y, s, m);
    input x, y, s;
    output m;

    assign m = (~s&x)|(s&y);
endmodule
