module Parte5(SW, HEX0, HEX1, HEX2, HEX3, HEX4);
    input [17:0]SW;
    output [0:6]HEX0;
    output [0:6]HEX1;
    output [0:6]HEX2;
    output [0:6]HEX3;
    output [0:6]HEX4;
     
    decodificador d1(SW[17:15], SW[14:0], HEX0, HEX1, HEX2, HEX3, HEX4);
endmodule

module decodificador(s, c, d0, d1, d2, d3, d4);
    //Chaves:
    input [2:0]s;
    input [14:0]c;
    
    //Displays fisicos
    output [6:0]d0;
    output [6:0]d1;
    output [6:0]d2;
    output [6:0]d3;
    output [6:0]d4;

    //Representacao de caracteres
    /*reg [2:0]H = 3'b000;
    reg [2:0]E = 3'b001;
    reg [2:0]L = 3'b010;
    reg [2:0]O = 3'b011;*/
    
    wire [2:0]H = c[14:12];
    wire [2:0]E = c[11:9];
    wire [2:0]L = c[8:6];
    wire [2:0]L2 = c[5:3];
    wire [2:0]O = c[2:0];

    //Displays logicos
    wire [2:0]H0;
    wire [2:0]H1;
    wire [2:0]H2;
    wire [2:0]H3;
    wire [2:0]H4;
    
    //Atribuicao de caracteres aos displays logicos
    mux_3bit_5to1 m1(H, E, L, L2, O, s, H4);
    mux_3bit_5to1 m2(E, L, L2, O, H, s, H3);
    mux_3bit_5to1 m3(L, L2, O, H, E, s, H2);
    mux_3bit_5to1 m4(L2, O, H, E, L, s, H1);
    mux_3bit_5to1 m5(O, H, E, L, L2, s, H0);
    
    //Atrbuindo aos displays fisicos os valores dos displays logicos
    char_7seg c1(H4[2], H4[1], H4[0], d4);
    char_7seg c2(H3[2], H3[1], H3[0], d3);
    char_7seg c3(H2[2], H2[1], H2[0], d2);
    char_7seg c4(H1[2], H1[1], H1[0], d1);
    char_7seg c5(H0[2], H0[1], H0[0], d0);
endmodule

module char_7seg(c2, c1, c0, D);
    input c2;
    input c1;
    input c0;
    output [0:6]D;
    
    wire h = (~c2&~c1&~c0);
    wire e = (~c2&~c1&c0);
    wire l = (~c2&c1&~c0);
    wire o = (~c2&c1&c0);

    assign D[6] = ~(h|e);
    assign D[5] = ~(h|e|l|o);
    assign D[4] = ~(h|e|l|o);
    assign D[3] = ~(e|l|o);
    assign D[2] = ~(h|o);
    assign D[1] = ~(h|o);
    assign D[0] = ~(e|o);
endmodule

module mux_3bit_5to1(u, v, w, x, y, s, m);
    input [2:0]u;
    input [2:0]v;
    input [2:0]w;
    input [2:0]x;
    input [2:0]y;
    input [2:0]s;
    output [2:0]m;

    mult5p1 m1(u[2], v[2], w[2], x[2], y[2], s[0], s[1], s[2], m[2]);
    mult5p1 m2(u[1], v[1], w[1], x[1], y[1], s[0], s[1], s[2], m[1]);
    mult5p1 m3(u[0], v[0], w[0], x[0], y[0], s[0], s[1], s[2], m[0]);
endmodule

module mult5p1(u, v, w, x, y, s0, s1, s2, m);
    input u, v, w, x, y, s0, s1, s2;
    output m;
    
    mult2p1 m1(u, v, s0, a);
    mult2p1 m2(w, x, s0, b);
    mult2p1 m3(a, b, s1, c);
    mult2p1 m4(c, y, s2, m);
endmodule

module mult2p1(x, y, s, m);
    input x, y, s;
    output m;
    
    assign m = (~s&x)|(s&y);
endmodule

