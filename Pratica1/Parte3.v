module Parte3(SW, LEDG, LEDR);
   input [17:0]SW;
   output [8:0]LEDG;
   output [17:0]LEDR;
    
   assign LEDR = SW;
   x3mult5p1 m1(SW[14:12], SW[11:9], SW[8:6], SW[5:3], SW[2:0], SW[17], SW[16], SW[15], LEDG[2:0]);
endmodule

module x3mult5p1(u, v, w, x, y, s0, s1, s2, m);
   input [2:0]u;
   input [2:0]v;
   input [2:0]w;
   input [2:0]x;
   input [2:0]y;
   input s0, s1, s2;
   output [2:0]m;
    
   mult5p1 m1(u[2], v[2], w[2], x[2], y[2], s0, s1, s2, m[2]);
   mult5p1 m2(u[1], v[1], w[1], x[1], y[1], s0, s1, s2, m[1]);
   mult5p1 m3(u[0], v[0], w[0], x[0], y[0], s0, s1, s2, m[0]);
endmodule

module mult5p1(u, v, w, x, y, s0, s1, s2, m);
   input u, v, w, x, y, s0, s1, s2;
   output m;
    
   mult2p1 m1(u, v, s0, a);
   mult2p1 m2(w, x, s0, b);
   mult2p1 m3(a, b, s1, c);
   mult2p1 m4(c, y, s2, m);
endmodule

module mult2p1(x, y, s, m);
   input x, y, s;
   output m;
    
   assign m = (~s&x)|(s&y);
endmodule
