module Pratica0(SW, LEDR);
    input [17:0]SW;
    output [17:0]LEDR;
    
    interruptor_paralelo ip (SW[0], SW[1], LEDR[0]);
endmodule

module interruptor_paralelo (x1, x2, f);
    input x1, x2;
    output f;
    assign f= x1 & ~x2 | ~x1 & x2;
endmodule
