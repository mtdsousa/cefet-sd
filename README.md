Laboratório de Sistemas Digitais
==================
Sobre o Curso
-------------------
Curso ministrado aos estudantes do segundo período de Engenharia de Computação do Centro Federal de Educação Tecnológica (CEFET) de Minas Gerais.

  * **Professora**: [Daniela Cascini][1] 
  * **Referência**: Widmer, Neal S.; Widmer, Neal S.; Tocci, Ronald J.; Sistemas Digitais - Princípios e Aplicações
  * **Plataforma de Desenvolvimento**: [Altera® DE2 Development and Education board][2] - Altera Cyclone® II 2C35 FPGA
  * **Práticas**
    * Prática 0 - Introdução ao Verilog e à DE2
    * Prática 1 - Chaves, LEDs e Multiplexadores
    * Prática 2 - Latches, Flip-flops e Registradores
    * Prática 3 - Números e Displays
    * Prática 4 - Contadores, Somadores e Subtratores
  * **Avaliações**
    * Prova I
    * Prova II
    * Prova Suplementar - Clock
 
  [1]: http://homepages.dcc.ufmg.br/~cascini/
  [2]: http://www.altera.com/education/univ/materials/boards/de2/unv-de2-board.html
