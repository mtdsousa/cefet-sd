module Q01(SW, KEY, HEX1, HEX0, LEDR);  
    input [0:0]SW;
    input [0:0]KEY;
    output [6:0]HEX1;
    output [6:0]HEX0;
    output [3:0]LEDR;
    
    wire Qa, Qb, Qc, Qd;
    wire reset;
    
    assign reset = (Qd&Qc&~Qb&Qa) || SW[0];
    
    ffT ffTa(1, ~reset, KEY[0], Qa);
    ffT ffTb(Qa, ~reset, KEY[0], Qb);
    ffT ffTc(Qa&Qb, ~reset, KEY[0], Qc);
    ffT ffTd((Qa&Qb)&Qc, ~reset, KEY[0], Qd);
    
    assign LEDR[0] = Qa;
    assign LEDR[1] = Qb;
    assign LEDR[2] = Qc;
    assign LEDR[3] = Qd;
    
    wire [3:0]d;
    wire [3:0]u;
    BCD b1({Qe, Qd, Qc, Qb, Qa}, d, u);

    char_7seg c1(d, HEX1);
    char_7seg c0(u, HEX0);
    
endmodule

module ffT(T, clear, clk, Q);
    input T, clear, clk;
    output reg Q;
    
    always @(posedge clk or negedge clear) begin
        if (~clear) begin
            Q <= 1'b0;
        end else if (T) begin
            Q <= (T&~Q )|(~T&Q);
        end
    end
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [0:6]D;
    
    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule

module BCD(input [3:0]binary, output reg [3:0]Tens, output reg [3:0]Ones);
    integer i;
    always @(binary) begin
        Tens = 4'd0;
        Ones = 4'd0;
        
        for(i=3; i>=0; i=i-1) begin
            if (Tens>=5)
                Tens = Tens + 3;
            if (Ones>=5)
                Ones = Ones + 3;
            Tens = Tens << 1;
            Tens[0] = Ones[3];
            Ones = Ones << 1;
            Ones[0] = binary[i];
        end
    end
endmodule
