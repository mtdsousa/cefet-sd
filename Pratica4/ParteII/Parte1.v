module Parte1(SW, KEY, LEDR, LEDG, HEX7, HEX6, HEX5, HEX4, HEX3, HEX2, HEX1, HEX0);
    input [15:0]SW;
    input [1:0]KEY;
    output [7:0]LEDR;
    output [8:8]LEDG;
    
    output [6:0]HEX7;
    output [6:0]HEX6;
    output [6:0]HEX5;
    output [6:0]HEX4;
    output [6:0]HEX3;
    output [6:0]HEX2;
    output [6:0]HEX1;
    output [6:0]HEX0;
    
    ffd a1(SW[15], KEY[1], KEY[0], a1d);
    ffd a2(SW[14], KEY[1], KEY[0], a2d);
    ffd a3(SW[13], KEY[1], KEY[0], a3d);
    ffd a4(SW[12], KEY[1], KEY[0], a4d);
    ffd a5(SW[11], KEY[1], KEY[0], a5d);
    ffd a6(SW[10], KEY[1], KEY[0], a6d);
    ffd a7(SW[9], KEY[1], KEY[0], a7d);
    ffd a8(SW[8], KEY[1], KEY[0], a8d);
    
    ffd b1(SW[7], KEY[1], KEY[0], b1d);
    ffd b2(SW[6], KEY[1], KEY[0], b2d);
    ffd b3(SW[5], KEY[1], KEY[0], b3d);
    ffd b4(SW[4], KEY[1], KEY[0], b4d);
    ffd b5(SW[3], KEY[1], KEY[0], b5d);
    ffd b6(SW[2], KEY[1], KEY[0], b6d);
    ffd b7(SW[1], KEY[1], KEY[0], b7d);
    ffd b8(SW[0], KEY[1], KEY[0], b8d);
    
    somador s0(a8d, b8d, 0, s7r, co0);
    somador s1(a7d, b7d, co0, s6r, co1);
    somador s2(a6d, b6d, co1, s5r, co2);
    somador s3(a5d, b5d, co2, s4r, co3);
    somador s4(a4d, b4d, co3, s3r, co4);
    somador s5(a3d, b3d, co4, s2r, co5);
    somador s6(a2d, b2d, co5, s1r, co6);
    somador s7(a1d, b1d, co6, s0r, co7);
    
    ffd sd1(s0r, KEY[1], KEY[0], s1d);
    ffd sd2(s1r, KEY[1], KEY[0], s2d);
    ffd sd3(s2r, KEY[1], KEY[0], s3d);
    ffd sd4(s3r, KEY[1], KEY[0], s4d);
    ffd sd5(s4r, KEY[1], KEY[0], s5d);
    ffd sd6(s5r, KEY[1], KEY[0], s6d);
    ffd sd7(s6r, KEY[1], KEY[0], s7d);
    ffd sd8(s7r, KEY[1], KEY[0], s8d);
    
    ffd o(co7, KEY[1], KEY[0], od);
    
    assign LEDR[7:0] = {s1d, s2d, s3d, s4d, s5d, s6d, s7d, s8d};
    assign LEDG = od;
    
    wire [3:0]ha;
    wire [3:0]ta;
    wire [3:0]oa;
    
    wire [3:0]hb;
    wire [3:0]tb;
    wire [3:0]ob;

    wire [3:0]hs;
    wire [3:0]ts;
    wire [3:0]os;
    
    BCD a({a1d, a2d, a3d, a4d, a5d, a6d, a7d, a8d}, ha, ta, oa);
    BCD b({b1d, b2d, b3d, b4d, b5d, b6d, b7d, b8d}, hb, tb, ob);
    BCD s({s1d, s2d, s3d, s4d, s5d, s6d, s7d, s8d}, hs, ts, os);
    
    char_7seg h7(ha, HEX7);
    char_7seg h6(ta, HEX6);
    char_7seg h5(oa, HEX5);
    
    char_7seg h4(hb, HEX4);
    char_7seg h3(tb, HEX3);
    char_7seg h2(ob, HEX2);
    
    char_7seg h1(ts, HEX1);
    char_7seg h0(os, HEX0);
    
endmodule

module ffd(d, clk, rst, r);
    input d, clk, rst;
    output reg r;
    always@(rst) begin
        if (~rst) begin
            r = 1'b0;
        end else if(~clk) begin
            r = d;
        end
    end
endmodule

module mux(a, b, s, r);
    input a, b, s;
    output r;
    
    assign r = (a&~s)|(b&s);
endmodule

module somador(a, b, ci, s, co);
    input a, b, ci;
    output s, co;
    
    wire t1;
    
    assign t1 = (a&~b)|(~a&b);
    assign s = (t1&~ci)|(~t1&ci);
    mux m(b, ci, t1, co);
endmodule

module BCD(input [7:0]binary, output reg [3:0]Hundreds, output reg [3:0]Tens, output reg [3:0]Ones);
    integer i;
    always @(binary) begin
        Hundreds = 4'd0;
        Tens = 4'd0;
        Ones = 4'd0;
        
        for(i=7; i>=0; i=i-1) begin
            if (Hundreds>=5)
                Hundreds = Hundreds + 3;
            if (Tens>=5)
                Tens = Tens + 3;
            if (Ones>=5)
                Ones = Ones + 3;
            Hundreds = Hundreds << 1;
            Hundreds[0] = Tens[3];
            Tens = Tens << 1;
            Tens[0] = Ones[3];
            Ones = Ones << 1;
            Ones[0] = binary[i];
        end
    end
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [0:6]D;
    
    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule

