module Parte3(SW, KEY, HEX3, HEX2, HEX1, HEX0);
    input [1:0]SW;
    input [0:0]KEY;
    output [6:0]HEX3;
    output [6:0]HEX2;
    output [6:0]HEX1;
    output [6:0]HEX0;

    wire [15:0]Q;
    wire [3:0]m;
    wire [3:0]c;
    wire [3:0]d;
    wire [3:0]u;

    lpmCounter lc1(~SW[0], SW[1], KEY[0], Q);   
    BCD b1(Q, m, c, d, u);

    char_7seg c3(m, HEX3);
    char_7seg c2(c, HEX2);
    char_7seg c1(d, HEX1);
    char_7seg c0(u, HEX0);
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [0:6]D;

    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule

module BCD(input [15:0]binary, output reg [3:0]Thousands, output reg [3:0]Hundreds, output reg [3:0]Tens, output reg [3:0]Ones);
    integer i;
    always @(binary) begin
        Thousands = 4'd0;
        Hundreds = 4'd0;
        Tens = 4'd0;
        Ones = 4'd0;
        
        for(i=15; i>=0; i=i-1) begin
            if(Thousands>=5)
                Thousands = Thousands + 3;
            if (Hundreds>=5)
                Hundreds = Hundreds + 3;
            if (Tens>=5)
                Tens = Tens + 3;
            if (Ones>=5)
                Ones = Ones + 3;
            Thousands = Thousands << 1;
            Thousands[0] = Hundreds[3];
            Hundreds = Hundreds << 1;
            Hundreds[0] = Tens[3];
            Tens = Tens << 1;
            Tens[0] = Ones[3];
            Ones = Ones << 1;
            Ones[0] = binary[i];
        end
    end
endmodule

