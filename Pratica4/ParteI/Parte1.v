module Parte1(SW, LEDR, KEY, HEX3, HEX2, HEX1, HEX0);
    input [1:0]SW;
    input [3:0]KEY;
    output [6:0]HEX3;
    output [6:0]HEX2;
    output [6:0]HEX1;
    output [6:0]HEX0;
    output [15:0]LEDR;

    wire Qa, Qb, Qc, Qd, Qe, Qf, Qg, Qh, Qi, Qj, Qk, Ql, Qm, Qn, Qo, Qp;

    ffT ffTa(SW[1], SW[0], KEY[0], Qa);
    ffT ffTb(SW[1]&Qa, SW[0], KEY[0], Qb);
    ffT ffTc((SW[1]&Qa)&Qb, SW[0], KEY[0], Qc);
    ffT ffTd(((SW[1]&Qa)&Qb)&Qc, SW[0], KEY[0], Qd);

    ffT ffTe((((SW[1]&Qa)&Qb)&Qc)&Qd, SW[0], KEY, Qe);
    ffT ffTf(((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe, SW[0], KEY, Qf);
    ffT ffTg((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf, SW[0], KEY, Qg);
    ffT ffTh(((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg, SW[0], KEY, Qh);
    ffT ffTi((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh, SW[0], KEY, Qi);
    ffT ffTj(((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi, SW[0], KEY, Qj);
    ffT ffTk((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj, SW[0], KEY, Qk);
    ffT ffTl(((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj)&Qk, SW[0], KEY, Ql);
    ffT ffTm((((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj)&Qk)&Ql, SW[0], KEY, Qm);
    ffT ffTn(((((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj)&Qk)&Ql)&Qm, SW[0], KEY, Qn);
    ffT ffTo((((((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj)&Qk)&Ql)&Qm)&Qn, SW[0], KEY, Qo);
    ffT ffTp((((((((((((((((SW[1]&Qa)&Qb)&Qc)&Qd)&Qe)&Qf)&Qg)&Qh)&Qi)&Qj)&Qk)&Ql)&Qm)&Qn)&Qo), SW[0], KEY, Qp);

    assign LEDR[0] = Qa;
    assign LEDR[1] = Qb;
    assign LEDR[2] = Qc;
    assign LEDR[3] = Qd;
    assign LEDR[4] = Qe;
    assign LEDR[5] = Qf;
    assign LEDR[6] = Qg;
    assign LEDR[7] = Qh;
    assign LEDR[8] = Qi;
    assign LEDR[9] = Qj;
    assign LEDR[10] = Qk;
    assign LEDR[11] = Ql;
    assign LEDR[12] = Qm;
    assign LEDR[13] = Qn;
    assign LEDR[14] = Qo;
    assign LEDR[15] = Qp;

    wire [3:0]m;
    wire [3:0]c;
    wire [3:0]d;
    wire [3:0]u;
    BCD b1({Qp, Qo, Qn, Qm, Ql, Qk, Qj, Qi, Qh, Qg, Qf, Qe, Qd, Qc, Qb, Qa}, m, c, d, u);

    char_7seg c3(m, HEX3);
    char_7seg c2(c, HEX2);
    char_7seg c1(d, HEX1);
    char_7seg c0(u, HEX0);

endmodule

module ffT(T, clear, clk, Q);
    input T, clear, clk;
    output reg Q;

    always @(posedge clk or negedge clear) begin
        if (~clear) begin
            Q <= 1'b0;
        end else if (T) begin
            Q <= (T&~Q )|(~T&Q);
        end
    end
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [0:6]D;
    
    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule

module BCD(input [15:0]binary, output reg [3:0]Thousands, output reg [3:0]Hundreds, output reg [3:0]Tens, output reg [3:0]Ones);
    integer i;
    always @(binary) begin
        Thousands = 4'd0;
        Hundreds = 4'd0;
        Tens = 4'd0;
        Ones = 4'd0;

        for(i=15; i>=0; i=i-1) begin
            if(Thousands>=5)
                Thousands = Thousands + 3;
            if (Hundreds>=5)
                Hundreds = Hundreds + 3;
            if (Tens>=5)
                Tens = Tens + 3;
            if (Ones>=5)
                Ones = Ones + 3;
            Thousands = Thousands << 1;
            Thousands[0] = Hundreds[3];
            Hundreds = Hundreds << 1;
            Hundreds[0] = Tens[3];
            Tens = Tens << 1;
            Tens[0] = Ones[3];
            Ones = Ones << 1;
            Ones[0] = binary[i];
        end
    end
endmodule

