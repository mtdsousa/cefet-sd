module Parte2(SW, LEDR, HEX0, HEX1);
    input [3:0] SW;
    output [0:6] HEX0;
    output [0:6] HEX1;
    output [3:0]LEDR;

    assign LEDR = SW;

    wire [2:0]r2;
    wire [3:0]d0;

    comparator c(SW, r1);
    circuitA cA(SW[2:0], r2);
    circuitB cB(r1, HEX1);

    mult2p1 ma(SW[3], 0, r1, d0[3]);
    mult2p1 mb(SW[2], r2[2], r1, d0[2]);
    mult2p1 mc(SW[1], r2[1], r1, d0[1]);
    mult2p1 md(SW[0], r2[0], r1, d0[0]);

    char_7seg c7(d0, HEX0);
endmodule

module mult2p1(x, y, s, m);
    input x, y, s;
    output m;
    
    assign m = (~s&x)|(s&y);
endmodule

module comparator(c, R);
    input [3:0]c;
    output R;
    assign R = c[3]&(c[2]|c[1]);
endmodule

module circuitA(c, R);
    input [2:0]c;
    output [2:0]R;

    assign R[2] = (c[2]&c[1]);
    assign R[1] = (c[2]&~c[1]);
    assign R[0] = (c[0]);
endmodule

module circuitB(c, R);
    input c;
    output [0:6]R;

    //Para c=0: R=1000000. Para c=1: R=1111001
    //Casos iguais para qualquer valor de C
    assign R[6] = 1;
    assign R[2] = 0;
    assign R[1] = 0;

    mult2p1 ma(0, 1, c, R[5]);
    mult2p1 mb(0, 1, c, R[4]);
    mult2p1 mc(0, 1, c, R[3]);
    mult2p1 md(0, 1, c, R[0]);
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [6:0]D;

    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule

