module Parte1(SW, LEDR, HEX0, HEX1, HEX2, HEX3);
    output [6:0] HEX0;
    output [6:0] HEX1;
    output [6:0] HEX2;
    output [6:0] HEX3;
    output [17:0] LEDR;
    input [17:0] SW;
    
    assign LEDR = SW;
    char_7seg h3(SW[15:12], HEX3);
    char_7seg h2(SW[11:8], HEX2);
    char_7seg h1(SW[7:4], HEX1);
    char_7seg h0(SW[3:0], HEX0);
endmodule

module char_7seg(c, D);
    input [3:0]c;
    output [0:6]D;
    
    wire zero = (~c[3]&~c[2]&~c[1]&~c[0]);
    wire one = (~c[3]&~c[2]&~c[1]&c[0]);
    wire two = (~c[3]&~c[2]&c[1]&~c[0]);
    wire tree = (~c[3]&~c[2]&c[1]&c[0]);
    wire four = (~c[3]&c[2]&~c[1]&~c[0]);
    wire five = (~c[3]&c[2]&~c[1]&c[0]);
    wire six = (~c[3]&c[2]&c[1]&~c[0]);
    wire seven = (~c[3]&c[2]&c[1]&c[0]);
    wire eight = (c[3]&~c[2]&~c[1]&~c[0]);
    wire nine = (c[3]&~c[2]&~c[1]&c[0]);

    assign D[0] = ~(two | tree | four | five | six | eight | nine);
    assign D[5] = ~(one | two | tree | four | seven | eight | nine | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | eight | nine | zero);
    assign D[3] = ~(two | tree | five | six | eight | nine | zero);
    assign D[2] = ~(two | six | eight | zero);
    assign D[1] = ~(four | five | six | seven | eight | nine | zero);
    assign D[6] = ~(two | tree | five | six | seven | eight | nine | zero);
endmodule
