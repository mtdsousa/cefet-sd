module Parte3(SW, LEDR, LEDG);
    input [8:0]SW;
    output [8:0] LEDR;
    output [4:0] LEDG;

    wire [3:0]s;
    wire c4;

    somador s0(SW[8], SW[4], SW[0], s[0], c1);
    somador s1(c1, SW[5], SW[1], s[1], c2);
    somador s2(c2, SW[6], SW[2], s[2], c3);
    somador s3(c3, SW[7], SW[3], s[3], c4);

    assign LEDR = SW;
    assign LEDG[4] = c4;
    assign LEDG[3:0] = s[3:0];

endmodule

module somador(ci, a, b, s, co);
    wire r1;
    input ci, a, b;
    output s, co;

    assign r1 = ((a&~b)|(~a&b));
    assign s = ((r1&~ci)|(~r1&ci));
    mult2p1 m(b, ci, r1, co);
endmodule

module mult2p1(x, y, s, m);
    input x, y, s;
    output m;
    
    assign m = (~s&x)|(s&y);
endmodule

