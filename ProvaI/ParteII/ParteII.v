//Fonte: http://www.cin.ufpe.br/~voo/sd/Aula6.Verilog.pdf
module FFJK_ (J, K, Clk, q);
    input J, K, Clk;
    output reg q;
    
    initial begin
        q = 1'b0;
    end
    
    always@(negedge Clk) begin
        if(J==1'b0 && K==1'b0)
            q = q;
        else if(J==1'b1 && K==1'b0)
            q = 1'b1;
        else if(J==1'b0 && K==1'b1)
            q = 1'b0;
        else if(J==1'b1 && K==1'b1)
            q = ~q;
    end
endmodule

module char_7seg(c2, c1, c0, D);
    input c2, c1, c0;
    output [0:6]D;
    
    wire zero = (~c2&~c1&~c0);
    wire one = (~c2&~c1&c0);
    wire two = (~c2&c1&~c0);
    wire tree = (~c2&c1&c0);
    wire four = (c2&~c1&~c0);
    wire five = (c2&~c1&c0);
    wire six = (c2&c1&~c0);
    wire seven = (c2&c1&c0);

    assign D[0] = ~(two | tree | four | five | six);
    assign D[5] = ~(one | two | tree | four | seven | zero);
    assign D[4] = ~(one | tree | four | five | six | seven | zero);
    assign D[3] = ~(two | tree | five | six | zero);
    assign D[2] = ~(two | six | zero);
    assign D[1] = ~(four | five | six | seven | zero);
    assign D[6] = ~(two | tree | five | six | seven | zero);
endmodule

module ParteII(KEY, HEX0, LEDR);
    input [3:0] KEY;
    output [6:0] HEX0;
    output [17:0] LEDR;   
    
    //Contador:
    FFJK_ c0(1'b1, 1'b1, KEY[0], C0); //FF LSB
    FFJK_ c1(1'b1, 1'b1, C0, C1);
    FFJK_ c2(1'b1, 1'b1, C1, C2); //FF MSB
    assign LEDR[17] = C2;
    assign LEDR[16] = C1;
    assign LEDR[15] = C0;
    char_7seg c7s(C2, C1, C0, HEX0);
endmodule


    
