//Fonte: http://www.cin.ufpe.br/~voo/sd/Aula6.Verilog.pdf
module FFJK_ (J, K, Clk, q);
    input J, K, Clk;
    output reg q;
    
    initial begin
        q = 1'b0;
    end
    
    always@(posedge Clk) begin
        if(J==1'b0 && K==1'b0)
            q = q;
        else if(J==1'b1 && K==1'b0)
            q = 1'b1;
        else if(J==1'b0 && K==1'b1)
            q = 1'b0;
        else if(J==1'b1 && K==1'b1)
            q = ~q;
    end
endmodule

module ParteI(SW, LEDR);
    input [17:0]SW;
    output [17:0]LEDR;
    
    FFJK_ ffjk1(SW[1], SW[0], SW[17], LEDR[0]);
endmodule              
