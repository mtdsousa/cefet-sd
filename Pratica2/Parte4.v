module LatchD (s, c, l);
    input s;
    input c;
    output l;

    wire R_g,S_g,Qa,Qb/*synthesis keep*/;
    wire R = ~(s);
    wire S = s;
    wire Q;
    wire Clk = c;
    assign R_g=~(R & Clk);
    assign S_g=~(S & Clk);
    assign Qa=~(S_g & Qb);
    assign Qb=~(R_g & Qa);

    assign Q= Qa;
    assign l = Q;
endmodule

module LatchDA (D, Clk, Q);
    input D, Clk;
    output reg Q;

    always@(D, Clk)
        if(Clk)
            Q = D;
endmodule

module Parte4(SW, LEDR);
    input [17:0]SW;
    output [17:0]LEDR;

    LatchD a (SW[0], SW[17], LEDR[0]);
    LatchDA b (SW[1], SW[17], LEDR[1]);
    LatchDA c (SW[2], ~SW[17], LEDR[2]);
endmodule

