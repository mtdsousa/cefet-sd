module Parte2 (SW, LEDR);
    input [17:0]SW;
    output [17:0]LEDR;

    wire R_g,S_g,Qa,Qb/*synthesis keep*/;
    wire R = ~(SW[0]);
    wire S = SW[0];
    wire Q;
    wire Clk = SW[1];
    assign R_g=~(R & Clk);
    assign S_g=~(S & Clk);
    assign Qa=~(S_g & Qb);
    assign Qb=~(R_g & Qa);

    assign Q= Qa;
    assign LEDR[0] = Q;
endmodule 
