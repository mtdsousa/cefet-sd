module LatchD (s, c, l);
    input s;
    input c;
    output l;

    wire R_g,S_g,Qa,Qb/*synthesis keep*/;
    wire R = ~(s);
    wire S = s;
    wire Q;
    wire Clk = c;
    assign R_g=~(R & Clk);
    assign S_g=~(S & Clk);
    assign Qa=~(S_g & Qb);
    assign Qb=~(R_g & Qa);
    assign Q= Qa;
    assign l = Q;
endmodule 

module Parte3 (SW, LEDR);
    input [17:0]SW;
    output [17:0]LEDR;

    LatchD a (SW[0], ~SW[1], Q);
    LatchD b (Q, SW[1], LEDR[0]);
endmodule
